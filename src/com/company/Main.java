package com.company;

import java.util.Scanner;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        System.out.println("enter a sequence of integers seperated by space: ");
        Scanner scanner = new Scanner(System.in);
        String sequence = scanner.nextLine();
        String[] tokens = sequence.split(" ");
        int[] seqArray = new int[tokens.length];

        int i = 0;
        for (String token : tokens){
            seqArray[i++] = Integer.parseInt(token);
        }

        Arrays.sort(seqArray);

        i = 0;
        boolean satisfied = true;
        int level = 0;
        int diff = 0;
        while (satisfied && i < seqArray.length -1 ){
            if ((seqArray[i+1] - seqArray[i]) == 0){
                i++;
                continue;
            }
            else if ((seqArray[i+1] - seqArray[i]) == diff){
                level++;
            }
            else if (diff == 0){
                diff = (seqArray[i+1] - seqArray[i]);
                level++;
            }
            else {
                satisfied = false;
            }
            if (level > 2){
                satisfied = false;
            }
            i++;
        }

        if (!satisfied){
            diff = -1;
        }

        System.out.println(diff);
    }
}
